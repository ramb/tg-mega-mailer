import os
import pickle
from telegram import Update, Bot, ParseMode
from telegram.ext import Updater
from typing import Callable, Dict, List
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
from imapclient import IMAPClient
from imapclient.exceptions import LoginError

from src.mail import Mail, EmailMsg

STORAGE_FILENAME: str = 'db.bin'


class UserData:
    username: str
    password: str

    def __init__(self, u, p):
        self.username = u
        self.password = p


class Worker:
    _mail_host: str
    _mail_port: int
    _users: Dict[int, UserData]
    _mailers: Dict[int, Mail]
    _scheduler: BackgroundScheduler
    _updater: Updater

    def __init__(self, updater: Updater, host: str, port: int):
        self._mail_host = host
        self._mail_port = port
        self._users = {}
        self._mailers = {}
        self._scheduler = BackgroundScheduler()
        self._scheduler.start()
        self._scheduler.add_job(self.check_mail, IntervalTrigger(seconds=30))
        self._scheduler.add_job(self.save_to_file, IntervalTrigger(minutes=1))
        self._updater = updater
        self.load_from_file()
        self.sync_mailers()

    def load_from_file(self):
        if os.path.isfile(STORAGE_FILENAME):
            with open(STORAGE_FILENAME, 'rb') as db:
                self._users = pickle.load(db)

    def save_to_file(self):
        with open(STORAGE_FILENAME, 'wb') as db:
            pickle.dump(self._users, db)

    def sync_mailers(self):
        for user_id, data in self._users.items():
            if not self._mailers.get(user_id):
                self._mailers.update({user_id: Mail(data.username, data.password, self._mail_host, self._mail_port)})

        for user_id, mailer in self._mailers.items():
            if not self._users.get(user_id):
                self._users.pop(user_id)

    def set(self, user_id: int, data: UserData):
        self._users.update({user_id: data})
        self.sync_mailers()

    def remove(self, user_id: int):
        if not self._users.get(user_id):
            return
        self._users.pop(user_id)
        self.sync_mailers()

    def check_mail(self):
        mailer: Mail
        for user_id, mailer in self._mailers.items():
            msgs: List[EmailMsg] = mailer.get_new_emails()
            if len(msgs) == 0:
                continue

            for msg in msgs:
                chat_msg: str = '*{}*\n_{}_\n{}\n'.format(msg.msg_from, msg.email, msg.subject)
                self._updater.bot.send_message(user_id, chat_msg, ParseMode.MARKDOWN)

    def check_credentials(self, login: str, password: str) -> bool:
        try:
            mail_client = IMAPClient(self._mail_host, self._mail_port, use_uid=True)
            mail_client.login(login, password)
            return True
        except LoginError:
            return False


def with_worker(func: Callable[[Bot, Update, Worker], None], worker: Worker):
    def wrapper(bot: Bot, update: Update):
        func(bot, update, worker)
    return wrapper
