from src import mailbot


def main() -> None:
    b = mailbot.MailBot()
    b.run()


if __name__ == '__main__':
    main()
