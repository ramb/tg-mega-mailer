from telegram import Update, Bot, Chat, Message, User
from src.worker import Worker, UserData


def start(_0: Bot, update: Update, _2: Worker):
    _: Chat = update.effective_chat
    msg: Message = update.message
    _: User = update.effective_user

    msg.reply_text('✅ We are ready to start.\n'
                   'To watch you mail send `/watch login password` to bot\n'
                   'To stop watching mail send `/stop` to bot')


def watch(_: Bot, update: Update, worker: Worker):
    command = '/watch '

    chat: Chat = update.effective_chat
    msg: Message = update.message
    msg_text: str = msg.text
    pos = msg_text.find(command)
    if pos == -1:
        msg.reply_text('✋🏻 Incorrect /watch syntax')
        return

    cmd = msg_text[pos+len(command):]
    parts = cmd.split(' ')
    if len(parts) < 2:
        msg.reply_text('✋🏻 Incorrect /watch syntax')
        return

    login = parts[0]
    password = parts[1]

    correct_credentials = worker.check_credentials(login, password)
    if not correct_credentials:
        msg.reply_text('✋🏻 Incorrect credentials')
        return

    worker.set(chat.id, UserData(login, password))
    msg.reply_text('👀 Watching')


def stop(_: Bot, update: Update, worker: Worker):
    chat: Chat = update.effective_chat
    worker.remove(chat.id)
    msg: Message = update.message
    msg.reply_text('🙈 Don\'t watching')
