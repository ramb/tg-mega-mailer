import re
from typing import List, Set
from imapclient import IMAPClient
from imapclient.response_types import Envelope
from imapclient.exceptions import IMAPClientError
from email.header import decode_header


EMAIL_RE = re.compile(r"(.+) <(.+)>")
NAME_RE = re.compile(r"\"(.+)\"")


class EmailMsg:
    msg_from: str
    email: str
    subject: str

    def __init__(self, f, s):
        try:
            (name, email) = EMAIL_RE.findall(f)[0]
            self.email = email

            res = NAME_RE.findall(name)
            if len(res) == 1:
                self.msg_from = res[0]
            else:
                self.msg_from = Mail.decode_string(name)
        except:
            self.msg_from = f
            self.email = ''

        self.subject = s


class Mail:
    _username: str
    _password: str

    _processed_uids: Set[int] = set()
    _mailboxes: List[str] = []
    _mail_client: IMAPClient

    def __init__(self, username: str, password: str, host: str, port: int):
        self._username = username
        self._password = password
        self._mail_client = IMAPClient(host, port, use_uid=True)
        self._mail_client.login(self._username, self._password)

        boxes = self._mail_client.list_folders(pattern='INBOX*')

        for (_, _, box_name) in boxes:
            self._mailboxes.append(box_name)

    def __del__(self):
        self._mail_client.logout()

    @staticmethod
    def decode_string(hdr: str) -> str:
        h = decode_header(hdr)
        if not h[0][1]:
            return h[0][0]
        else:
            return h[0][0].decode(h[0][1])

    def get_new_emails(self) -> List[EmailMsg]:
        all_messages_ids: List[int] = []
        received_messages: List[EmailMsg] = []

        for mailbox in self._mailboxes:
            try:
                select_info = self._mail_client.select_folder(mailbox)
            except IMAPClientError:
                continue

            messages_ids = self._mail_client.search(['UNSEEN'])
            all_messages_ids.extend(messages_ids)

            if len(messages_ids) == 0:
                continue

            to_fetch = [x for x in messages_ids if x not in self._processed_uids]
            items = self._mail_client.fetch(to_fetch, ['ENVELOPE']).items()
            for msg_id, data in items:
                self._processed_uids.add(msg_id)
                envelope: Envelope = data[b'ENVELOPE']
                from_msg = envelope.sender[0].__str__()
                subject = Mail.decode_string(envelope.subject.decode())
                received_messages.append(EmailMsg(from_msg, subject))

        return received_messages

