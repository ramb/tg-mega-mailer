from telegram.ext import Updater, Dispatcher, CommandHandler
import configparser
from src import handlers
from src.worker import with_worker, Worker


class MailBot:
    _updater: Updater
    _config: configparser.ConfigParser
    _worker: Worker

    def __init__(self):
        self._config = configparser.ConfigParser()
        self._config.read('config.ini')

        token: str = self._config['main']['token']
        mail_host: str = self._config['main']['host']
        mail_port: int = int(self._config['main']['port'])
        proxy_url: str = self._config['proxy']['url']
        proxy_username: str = self._config['proxy']['username']
        proxy_pwd: str = self._config['proxy']['password']

        self._updater = Updater(token, request_kwargs={
            'proxy_url': proxy_url,
            'urllib3_proxy_kwargs': {
                'username': proxy_username,
                'password': proxy_pwd,
            }
        })

        self._worker = Worker(self._updater, mail_host, mail_port)
        self._setup_handlers()

    def _setup_handlers(self):
        dp: Dispatcher = self._updater.dispatcher
        dp.add_handler(CommandHandler('start', with_worker(handlers.start, self._worker)))
        dp.add_handler(CommandHandler('watch', with_worker(handlers.watch, self._worker)))
        dp.add_handler(CommandHandler('stop', with_worker(handlers.stop, self._worker)))

    def run(self):
        self._updater.start_polling()
        self._updater.idle()
