FROM python:3.6-alpine
COPY src /opt/mega-mail/src
COPY config.ini.template /opt/mega-mail/config.ini
COPY requirements.txt /opt/mega-mail
COPY db.bin /opt/mega-mail/db.bin
WORKDIR /opt/mega-mail

RUN apk update
RUN apk add tzdata
RUN apk add musl-dev gcc libffi-dev openssl-dev
RUN pip3 install -r requirements.txt

RUN cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime
RUN echo "Europe/Berlin" > /etc/timezone

CMD ["python", "-m", "src.main"]
